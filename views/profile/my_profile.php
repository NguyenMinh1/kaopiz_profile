<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;

?>
<title>My Profile</title>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="box_img">
            <img id="img" src=<?php if ($list_img) echo Yii::getAlias('@web/web/img/' . $list_img[0]['image']);
            else echo Yii::getAlias('@web/web/images/base_img.jpg');
            ?>>
            <div class="row">
                <div class="col-md-4 col-sm-4"></div>
                <div class="col-md-1 col-sm-1">
                    <div class="prev"></div>
                </div>
                <div class="col-md-2 col-sm-2">
                    <a href=<?= Url::to(['/member/upload']) ?>><img class="upload"
                                                                    src="<?= Yii::getAlias('@web/web/images/upload.png') ?>"></a>
                </div>
                <div class="col-md-1 col-sm-1">
                    <div class="next"></div>
                </div>
                <div class="col-md-2 col-sm-2"></div>
                <div class="col-md-2 col-sm-2"><?php if($list_img){?><a id="delete" href="<?= Url::to(['/member/delete-img','image'=>$list_img[0]['image']])?>"><?= \yii\helpers\Html::button('Delete',['class'=>'btn btn-danger'])?></a><?php }?></div>
            </div>
        </div>
        <div class="profile">
            <?php $form = ActiveForm::begin() ?>
            <table>
                <tr>
                    <td>Name:</td>
                    <td><?= $form->field($model, 'name')->textInput(); ?></td>
                    <td>&nbsp &nbsp Sex:</td>
                    <td><?= $form->field($model, 'sex')->textInput(['placeholder' => 'Edit...']) ?></td>
                </tr>
                <tr>
                    <td>Birthday:</td>
                    <td><?= $form->field($model, 'birthday')->textInput(['placeholder' => 'Edit...']) ?></td>
                    <td>&nbsp &nbsp Email:</td>
                    <td><?= $form->field($model, 'email')->textInput(['placeholder' => 'Edit...']) ?></td>
                </tr>
                <tr>
                    <td>Address:</td>
                    <td><?= $form->field($model, 'address')->textInput(['placeholder' => 'Edit...']) ?></td>
                    <td>&nbsp &nbsp Phone Numbers:</td>
                    <td><?= $form->field($model, 'phone')->textInput(['placeholder' => 'Edit...']) ?></td>
                </tr>
                </tr>
                <tr>
                    <td>Height:</td>
                    <td><?= $form->field($model, 'height')->textInput(['placeholder' => 'Edit...']) ?></td>
                    <td>&nbsp &nbsp Weight:</td>
                    <td><?= $form->field($model, 'weight')->textInput(['placeholder' => 'Edit...']) ?></td>
                </tr>
                </tr>
                <tr>
                    <td>Country:</td>
                    <td><?= $form->field($model, 'country')->textInput(['placeholder' => 'Edit...']) ?></td>
                    <td>&nbsp &nbsp Nickname:</td>
                    <td><?= $form->field($model, 'nickname')->textInput(['placeholder' => 'Edit...']) ?></td>
                </tr>
                </tr>
                <tr>
                    <td>Sky:</td>
                    <td><?= $form->field($model, 'sky')->textInput(['placeholder' => 'Edit...']) ?></td>
                    <td>&nbsp &nbsp Facebook:</td>
                    <td><?= $form->field($model, 'facebook')->textInput(['placeholder' => 'Edit...']) ?></td>
                </tr>
                <tr>
                    <td>Marital:</td>
                    <td colspan="3"><?= $form->field($model, 'marital')->textInput(['placeholder' => 'Edit...']) ?></td>
                </tr>
                <tr>
                    <td>Hobby:</td>
                    <td colspan="3"><?= $form->field($model, 'hobby')->textInput(['placeholder' => 'Edit...']) ?></td>
                </tr>
                <tr>
                    <td>Forte:</td>
                    <td colspan="3"><?= $form->field($model, 'forte')->textInput(['placeholder' => 'Edit...']) ?></td>
                </tr>
                <tr>
                    <td>Foible:</td>
                    <td colspan="3"><?= $form->field($model, 'foible')->textInput(['placeholder' => 'Edit...']) ?></td>
                </tr>
                <tr align='left'>
                    <td colspan="2"></td>
                    <td colspan="2" id="tr_submit"></td>
                </tr>
            </table>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>

<div id="slideshowxx">
    <img class="activexx" src=<?= Yii::getAlias('@web/web/img/' . $img_slides[0]['image']) ?>>
    <?php for ($i = 1; $i < count($img_slides); $i++) { ?>
        <img src=<?= Yii::getAlias('@web/web/img/' . $img_slides[$i]['image']) ?>>
    <?php } ?>
</div>
<script type="text/javascript">


    var input = document.getElementsByTagName('input');
    var i = 0;
    for (i = 0; i < input.length; i++) {
        input[i].addEventListener('change', function () {

            var submit = document.getElementsByClassName('submit');
            if (submit.length == 0) {
                var submit = document.createElement("input");
                submit.type = 'submit';
                submit.value = "Save";
                submit.className = 'submit';

                var tr_submit = document.getElementById("tr_submit");
                tr_submit.appendChild(submit);
            }
        });
    }

</script>

<script type="text/javascript" src=<?= Yii::getAlias('@web/web/js/vendor/jquery-1.10.1.min.js') ?>></script>
<script type="text/javascript">

    function slideSwitch() {
        var $active = $('#slideshowxx IMG.activexx');

        if ($active.length == 0) $active = $('#slideshowxx IMG:last');

        // use this to pull the images in the order they appear in the markup
        var $next = $active.next().length ? $active.next()
            : $('#slideshowxx IMG:first');

// xóa ghi chú ở 3 dòng bên dưới nếu bạn muốn hiển thị ảnh ngẫu nhiên

        var $sibs = $active.siblings();
        var rndNum = Math.floor(Math.random() * $sibs.length);
        var $next = $($sibs[rndNum]);

        $active.addClass('last-activexx');

        $next.css({opacity: 0.0})
            .addClass('activexx')
            .animate({opacity: 1.0}, 1000, function () {
                $active.removeClass('activexx last-activexx');
            });
    }

    $(function () {
        setInterval("slideSwitch()", 10000);
    });

</script>
<script type="text/javascript">
    <?php if($list_img) {?>
    var list_img = Array(<?php
        foreach ($list_img as $img)
            echo '\'' . Yii::getAlias('@web/web/img/' . $img['image']) . '\',';
        ?>'null');
    list_img.pop();
    var image=Array(<?php
        foreach ($list_img as $img)
            echo '\'' .$img['image'] . '\',';
        ?>'null');
    image.pop();
    var prev = document.getElementsByClassName('prev')[0];
    var next = document.getElementsByClassName('next')[0];
    var delete_img=document.getElementById('delete');
    var img = document.getElementById('img');
    i = 0;
    var max = list_img.length - 1;
    prev.addEventListener('click', function () {
        i--;
        if (i == -1) i = max;
        img.src = list_img[i];
        delete_img .href='/kaopiz_profile/member/delete-img?image='+image[i];
    });
    next.addEventListener('click', function () {
        i++;
        if (i == max + 1) i = 0;
        img.src = list_img[i];
        delete_img .href='/kaopiz_profile/member/delete-img?image='+image[i];
    });
    <?php }?>

</script>

<style>
    #slideshowxx {
        position: relative;
        height: 350px;
        z-index: -1;
    }

    #slideshowxx IMG {
        position: absolute;
        top: 0;
        left: 0;
        z-index: 8;
        opacity: 0.0;
    }

    #slideshowxx IMG.activexx {
        z-index: 10;
        opacity: 1.0;
    }

    #slideshowxx IMG.last-activexx {
        z-index: 9;
    }

    #slideshowxx img {
        /* Set rules to fill background */
        min-height: 100%;
        min-width: 1024px;

        /* Set up proportionate scaling */
        width: 100%;
        height: auto;

        /* Set up positioning */
        position: fixed;
        top: 0;
        left: 0;
    }

    .box_img {
        width: 100%;
        background-color: #fff;
        border-radius: 20px;
        padding: 20px 0;
    }

    .box_img > img {
        width: 100%;
        margin-bottom: 20px;
    }

    .prev {
        background-image: url(<?= Yii::getAlias('@web/web/images/Prev.png')?>);
        width: 30px;
        height: 30px;
        background-size: cover;
        float: left;
        cursor: pointer;
    }

    .next {
        background-image: url(<?= Yii::getAlias('@web/web/images/Next.png')?>);
        width: 30px;
        height: 30px;
        background-size: cover;
        cursor: pointer;
        float: right;
    }

    .profile {
        width: 100%;
        background: #fff;
        border-radius: 20px;
        margin-top: 40px;
        padding: 30px;
    }

    .profile table {
        width: 100%;
    }

    .profile input {
        border: 0px;
        box-shadow: none;
    }

    .profile .submit {
        width: 200px;
    }

    .upload {
        width: 30px;
        height: 30px;
        margin: 0px 35px;
    }

</style>
