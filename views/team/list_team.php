<?php


use yii\helpers\Url;
use app\helpers\MyHelper;


?>
<title><?= $members[0]['team_name'] ?></title>
<div class="about content">
    <?php if ($list_team) { ?>
        <div class="row">
            <ul class="tabs">
                <?php foreach ($list_team as $team1): ?>
                    <li class="col-md-3 col-sm-3">
                        <a href=<?= Url::to(['/team/team', 'team_id' => $team1['id']]) ?> class="icon-item <?php if ($members[0]['team_id'] == $team1['id']) echo 'active'; ?>
                        ">
                        <b style="font-size:15px"><?= $team1['name'] ?></b>
                        </a> <!-- /.icon-item -->
                    </li>
                <?php endforeach; ?>
            </ul>
            <!-- /.tabs -->
            <div class="col-md-12 col-sm-12">
                <div id="tab4">
                    <div class="toggle-content text-center">
                        <h3>Decription</h3>

                        <p><?= $members[0]['desc'] ? $members[0]['desc'] : NONE ?></p>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-5 col-sm-5"></div>
                <div class="col-md-2 col-sm-2"><?php if ($join) { ?>
                        <div style="margin-top:30px"><a
                            href=<?= Url::to(['/member/join-team', 'team_id' => $members[0]['team_id']]) ?> class="jointeam">Join
                            Team</a></div><?php } ?>
                    <?php if ($leave) { ?>
                        <div style="margin-top:30px"><a
                            href=<?= Url::to(['/member/leave-team', 'team_id' => $members[0]['team_id']]) ?> class="jointeam">Leave
                            Team</a></div><?php } ?>
                </div>
                <div class="col-md-5 col-sm-5"></div>
            </div>
            <!-- /.col-md-12 -->
        </div>
        <div class="row">
            <?php $i = 0 ?>
            <?php foreach ($members as $member) {
                if ($member['id'] && $member['active'] == 1) {
                    $i = 1; ?>
                    <div class="col-md-3 col-sm-3">
                        <div class="member-item">
                            <div class="thumb">
                                <a href=<?= Url::to(['profile/member-profile', 'member_id' => $member['id']]) ?>>
                                    <img
                                        src=<?php if (empty($member['avatar'])) echo Yii::getAlias('@web/web/images/base_avatar.jpg');
                                    else echo Yii::getAlias('@web/web/img/' . $member['avatar']);
                                    ?>></a>
                            </div>
                            <h4><?= $member['name'] ? $member['name'] : NONE ?></h4>
                            <span>Age: </span><?php if ($member['birthday']) echo MyHelper::getAge($member['birthday']); else echo NONE; ?>
                            <br>
                            <span>Phone Numbers: </span><?= $member['phone'] ? $member['phone'] : NONE; ?>
                        </div>
                        <!-- /.member-item -->
                    </div>
                <?php }
            }
            ?>

        </div>
    <?php } else echo 'Not found team.' ?>
</div>
<div id="slideshowxx">
    <img class="activexx" src=<?= Yii::getAlias('@web/web/img/' . $img_slides[0]['image']) ?>>
    <?php for ($i = 1; $i < count($img_slides); $i++) { ?>
        <img src=<?= Yii::getAlias('@web/web/img/' . $img_slides[$i]['image']) ?>>
    <?php } ?>
</div>

<script type="text/javascript" src=<?= Yii::getAlias('@web/web/js/vendor/jquery-1.10.1.min.js') ?>></script>
<script type="text/javascript">

    function slideSwitch() {
        var $active = $('#slideshowxx IMG.activexx');

        if ($active.length == 0) $active = $('#slideshowxx IMG:last');

        // use this to pull the images in the order they appear in the markup
        var $next = $active.next().length ? $active.next()
            : $('#slideshowxx IMG:first');

// xóa ghi chú ở 3 dòng bên dưới nếu bạn muốn hiển thị ảnh ngẫu nhiên

        var $sibs = $active.siblings();
        var rndNum = Math.floor(Math.random() * $sibs.length);
        var $next = $($sibs[rndNum]);

        $active.addClass('last-activexx');

        $next.css({opacity: 0.0})
            .addClass('activexx')
            .animate({opacity: 1.0}, 1000, function () {
                $active.removeClass('activexx last-activexx');
            });
    }

    $(function () {
        setInterval("slideSwitch()", 10000);
    });

</script>
<style>
    #slideshowxx {
        position: relative;
        height: 350px;
        z-index: -1;
    }

    #slideshowxx IMG {
        position: absolute;
        top: 0;
        left: 0;
        z-index: 8;
        opacity: 0.0;
    }

    #slideshowxx IMG.activexx {
        z-index: 10;
        opacity: 1.0;
    }

    #slideshowxx IMG.last-activexx {
        z-index: 9;
    }

    #slideshowxx img {
        /* Set rules to fill background */
        min-height: 100%;
        min-width: 1024px;

        /* Set up proportionate scaling */
        width: 100%;
        height: auto;

        /* Set up positioning */
        position: fixed;
        top: 0;
        left: 0;
    }

</style>
