<?php

use app\assets\LayoutAsset;
use yii\helpers\Url;

LayoutAsset::register($this);
if (class_exists('yii\debug\Module')) {
    $this->off(\yii\web\View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="icon" type="image/x-icon" href=<?= Yii::getAlias('@web/web/images/short_icon.png') ?>>
    <?php $this->head(); ?>
</head>
<body>
<?php $this->beginBody(); ?>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to
    improve your experience.</p>
<![endif]-->


<div class="bg-overlay"></div>

<div class="container-fluid">
    <div class="row">

        <div class="col-md-4 col-sm-12">
            <div class="sidebar-menu">

                <div class="logo-wrapper">
                    <h1 class="logo">
                        <a href="#"><img src=<?= Yii::getAlias('@web/web/images/logo.png') ?>>
                            <span>Kaopiz - Keep Innovating</span></a>
                    </h1>
                </div>
                <!-- /.logo-wrapper -->

                <div class="menu-wrapper">
                    <ul class="menu">
                        <!--  <li><a class="homebutton" href="#">Home</a></li>-->
                        <li><a href=<?= Url::to(['/member/all-members']) ?>>Members</a></li>
                        <li><a href=<?= Url::to(['/team/team']) ?>>Team</a></li>
                        <li><a href=<?= Url::to(['/profile/my-profile']) ?>>My Profile</a></li>

                    </ul>
                    <!-- /.menu -->
                    <a title="Logout" href=<?= Url::to(['/member/logout']) ?> class="toggle-menu"><i
                            class="fa fa-power-off"></i></a>
                </div>
                <!-- /.menu-wrapper -->

                <!--Arrow Navigation-->


            </div>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.col-md-4 -->

        <div class="col-md-8 col-sm-12">
            <?= $content ?>
        </div>

    </div>
</div>

<!-- /.container-fluid -->

<?php $this->endBody() ?>



</body>
</html>
<?php $this->endPage() ?>
