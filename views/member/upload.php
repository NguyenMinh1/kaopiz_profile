<?php
/**
 * Created by PhpStorm.
 * User: MINH
 * Date: 7/12/2016
 * Time: 10:07 AM
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use karpoff\icrop\CropImageUpload;

?>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="upload_box">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
            <?= $form->field($model, 'name')->textInput(['placeholder' => 'Edit...']) ?>
            <?= $form->field($model, 'image')->fileInput()->widget(CropImageUpload::className(), ['options' => ['class' => 'btn btn-info']]) ?>
            <?= $form->field($model, 'is_avatar')->checkbox(['value'=>true]) ?>
            <?= Html::submitButton('Upload', ['class' => 'btn btn-primary']) ?>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
<div id="slideshowxx">
    <img class="activexx" src=<?= Yii::getAlias('@web/web/img/' . $img_slides[0]['image']) ?>>
    <?php for ($i = 1; $i < count($img_slides); $i++) { ?>
        <img src=<?= Yii::getAlias('@web/web/img/' . $img_slides[$i]['image']) ?>>
    <?php } ?>
</div>

<script type="text/javascript" src=<?= Yii::getAlias('@web/web/js/vendor/jquery-1.10.1.min.js') ?>></script>
<script type="text/javascript">

    function slideSwitch() {
        var $active = $('#slideshowxx IMG.activexx');

        if ($active.length == 0) $active = $('#slideshowxx IMG:last');

        // use this to pull the images in the order they appear in the markup
        var $next = $active.next().length ? $active.next()
            : $('#slideshowxx IMG:first');

// xóa ghi chú ở 3 dòng bên dưới nếu bạn muốn hiển thị ảnh ngẫu nhiên

        var $sibs = $active.siblings();
        var rndNum = Math.floor(Math.random() * $sibs.length);
        var $next = $($sibs[rndNum]);

        $active.addClass('last-activexx');

        $next.css({opacity: 0.0})
            .addClass('activexx')
            .animate({opacity: 1.0}, 1000, function () {
                $active.removeClass('activexx last-activexx');
            });
    }

    $(function () {
        setInterval("slideSwitch()", 10000);
    });

</script>
<style>
    #slideshowxx {
        position: relative;
        height: 350px;
        z-index: -1;
    }

    #slideshowxx IMG {
        position: absolute;
        top: 0;
        left: 0;
        z-index: 8;
        opacity: 0.0;
    }

    #slideshowxx IMG.activexx {
        z-index: 10;
        opacity: 1.0;
    }

    #slideshowxx IMG.last-activexx {
        z-index: 9;
    }

    #slideshowxx img {
        /* Set rules to fill background */
        min-height: 100%;
        min-width: 1024px;

        /* Set up proportionate scaling */
        width: 100%;
        height: auto;

        /* Set up positioning */
        position: fixed;
        top: 0;
        left: 0;
    }

</style>


