<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "join_team".
 *
 * @property integer $id
 * @property integer $member_id
 * @property integer $team_id
 * @property integer $active
 */
class JoinTeam extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'join_team';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'team_id', 'active'], 'required'],
            [['member_id', 'team_id', 'active'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'team_id' => 'Team ID',
            'active' => 'Active',
        ];
    }
}
