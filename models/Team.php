<?php

namespace app\models;

use app\models\JoinTeam;
use Yii;
use app\models\Member;
class Team extends base\Team {

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Tên Team',
            'desc' => 'Mô tả về Team',
        ];
    }

    public function getJoinTeam() {
        return $this->hasMany(JoinTeam::className(), ['team_id' => 'id']);
    }
}

?>