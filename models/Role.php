<?php 
namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
class Role extends base\Role{
	public static function isRole($id, $role_name){
		$role = Role::findOne(['id' => $id]);
		return ($role == null)? false: ($role->name == $role_name);
	}

	public static function getRoleId($role_name){
		$role = Role::findOne(['name' => $role_name]);
		return $role->id;
	}	

	public static function getRoleName($role_id){
		$role = Role::findOne(['id' => $role_id]);
		return $role->name;
	}

	public function search($params,$is_search){
		$query = Role::find();
			$dataProvider = new ActiveDataProvider([
				'query' => $query,
				'pagination' => [
					'pageSize' => 50,
				],
			]);
			if(!$this->validate() || $is_search == false){
				return $dataProvider;
			}
			$this->setAttributes($params);
			if(!empty($this->name)) $query->andFilterWhere(['like', 'name', $this->name]);
			return $dataProvider;
	}
}
?>