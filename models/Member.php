<?php

namespace app\models;

use Yii;

class Member extends base\Member implements \yii\web\IdentityInterface {

    public $username;
    public $password;
    public $authKey;
    public $accessToken;


    public static function checkMember($email, $name) {
        $member = Member::findOne(['email' => $email]);
        if ($member == null) {
            $newMem = new Member();
            $newMem->email = $email;
            $newMem->name = $name;
            $newMem->is_banned = 0;
            $newMem->role_id = Role::getRoleId('member');
            $newMem->save();
            $member = Member::findOne(['email' => $email]);
        }
        return $member;
    }

    public function attributeLabels() {
        return array_merge([
            'id' => '',
            'email' => '',
            'name' => '',
            'sex' => '',
            'avatar' => '',
            'birthday' => '',
            'address' => '',
            'phone' => '',
            'height' => '',
            'weight' => '',
            'country' => '',
            'hobby' => '',
            'forte' => '',
            'foible' => '',
            'nickname' => '',
            'marital' => '',
            'sky' => '',
            'facebook' => '',
            'role_id' => '',
        ]);
    }

    public function rules() {
        $rules = parent::rules();

        //$rules[] = [['sex', 'birthday', 'address', 'phone', 'country'], 'required', 'message' => 'Không được bỏ trống!'];
        return $rules;
    }



    public static function findById($id) {
        return Member::findOne(['id' => $id]);
    }

    public static function findByEmail($email) {
        return Member::findOne(['email' => $email]);
    }

    public static function findIdentity($id) {
        return Member::findOne(['id' => $id]);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        return static::findOne(['access_token' => $token]);
    }

    public function getId() {
        return $this->id;
    }

    public function getAuthKey() {
        return $this->authKey;
    }

    public function validateAuthKey($authKey) {
        return $this->getAuthKey() == $authKey;
    }

    public function login($duration) {
        if ($this->is_banned == 0) {
            Yii::$app->user->login($this, $duration);
        }
    }

}

?>