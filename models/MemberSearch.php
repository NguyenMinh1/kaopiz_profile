<?php 
	namespace app\models;
	use yii\data\ActiveDataProvider;
	class MemberSearch extends Member{
		public function rules(){
			return [
				['email', 'email'],
				[['email','name', 'country', 'sex', 'role_id'], 'safe'],
			];
		}

		public function search($params){
			$query = Member::find();
			$dataProvider = new ActiveDataProvider([
				'query' => $query,
				'pagination' => [
					'pageSize' => 50,
				],
			]);
			if(!$this->validate()){
				return $dataProvider;
			}
			$this->setAttributes($params);
			if(!empty($this->email)) $query->andFilterWhere(['like', 'email', $this->email]);
			if(!empty($this->name)) $query->andFilterWhere(['like', 'name', $this->name]);
			if(!empty($this->country)) $query->andFilterWhere(['like', 'country', $this->country]);
			if(!empty($this->sex)) $query->andFilterWhere(['sex' =>$this->sex]);
			if(!empty($this->role_id)) $query->andFilterWhere(['role_id' =>$this->role_id]);
			return $dataProvider;
		}
	}
 ?>