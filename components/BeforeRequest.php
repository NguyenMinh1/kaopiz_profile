<?php

namespace app\components;

use Yii;
use yii\helpers\Url;
use yii\web\Application;
use yii\base\Behavior;
use yii\base\Component;

class BeforeRequest extends Component
{

    public function init()
    {

        if (Yii::$app->user->isGuest)

        parent::init();
    }

}
