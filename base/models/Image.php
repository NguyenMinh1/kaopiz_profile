<?php

namespace app\base\models;

use Yii;
use app\base\models\base\Image;

/**
 * This is the model class for table "image".
 *
 * @property integer $id
 * @property string $name
 * @property integer $member_id
 */
class Image extends Image
{

}
